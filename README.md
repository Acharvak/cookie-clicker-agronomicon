Cookie Clicker Agronomicon
==========================
Playing [Cookie Clicker](http://orteil.dashnet.org/cookieclicker), doing some gardening and getting tired of looking up how to unlock all the plants and plant upgrades? Thinking what patterns to use, but doing the math yourself is boring? **Cookie Clicker Agronomicon** is the add-on you need! It will tell you lots of stuff about your garden, such as:

* What plants can grow in every tile next tick and the probability for each one
* The chance of a plant maturing, dying or being contaminated next tick
* What seeds you can unlock with what you have, and how to do it
* What upgrade a plant can drop, if you don’t have it yet
* ...and it can even play sound alerts when the garden needs your attention!

Except not here! Cookie Clicker Agronomicon has moved to the following address:
-----------------------------------------------------------------------------------------------------------------------------------

https://github.com/Acharvak/Cookie-Clicker-Agronomicon/

The decision to move has been made due to a change in policy on part of Bitbucket.org.

If you use this mod, please take action now:
--------------------------------------------

1. If you use a userscript to load the Agronomicon, uninstall it, go to [the new page](https://github.com/Acharvak/Cookie-Clicker-Agronomicon/), and install the new userscript.
2. If you use a console command every time, go to the [the new page](https://github.com/Acharvak/Cookie-Clicker-Agronomicon/) and start using the new console command.
3. If you have put links to this mod on other websites, start linking to [the new page](https://github.com/Acharvak/Cookie-Clicker-Agronomicon/).

**You should switch as soon as possible. This repository may stop working at some point after July 1st, 2020.**

Sorry for the inconvenience.

For the technically inclined, the change in policy I mentioned was the decision to stop supporting the Mercurial version control system. Without Mercurial, there is no point in staying on Bitbucket, we may as well migrate to GitHub. The script is still served from here to users with the old userscript through the Downloads section.

Agronomicon.js in this repository is the version 2.022 with a migration warning added.
